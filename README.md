# stamps2019

[https://hackmd.io/@astrobiomike/stamps2019](https://hackmd.io/@astrobiomike/stamps2019)

# Tutorial outline
1. [Part 1: MetagenomeScope](#part1)
 * [Example 1: Trial and error](#part1e1) 
 * [Example 2: Frayed rope](#part1e2)
 * [Example 3: SPQR ](#part1e3)

2. [Part 2: Reference-guided assembly with MetaCompass](#part2)
 * [Example 1: Reference-guided assembly with provided references](#part2e1) 
 * [Example 2: Reference-guided assembly without provided references](#part2e2)

3. [Part 3: Strain-level analyses with ParSNP](#part3)
 * [MERS Coronavirus](#part3e1)
 * [Staphylococcus genomes from HMP datasets](#part3e2)

4. [Part 4: AliView](#part4)

# <a name="part1">Refresher from Taylor's presentation </a>

-- Hard drive space (we will need ~15-20GB)

    df -h .

-- CPU

    nproc

-- RAM

    free -mh

-- FASTQ format

    @SRR1234
    ATCGA
    +SRR1234
    #!#!#

-- Downsample file (pseudorandom)

    head -n 4000000
    
# <a name="part1">Installing today's software on your laptop/at home institution </a>

-- MetaCompass (needs a server for large datasets):

    git clone https://github.com/IGS/Chiron.git
    ./Chiron/bin/metacompass_interactive

-- MetagenomeScope (web-based):
 
  [http://mgsc.umiacs.io](http://mgsc.umiacs.io)

--- ParSNP/Harvest (pre-built binaries can run on laptop, prefers servers for large datasets)

  [https://github.com/marbl/harvest/releases](https://github.com/marbl/harvest/releases)

# <a name="part1">Part 1: MetagenomeScope ([Marcus Fedarko](https://fedarko.github.io/)) </a>
 
========
# MetagenomeScope STAMPS Tutorial

(Inspired by [this SEPP/TIPP tutorial](https://github.com/MGNute/stamps-tutorial/blob/master/tutorial.md).)

There are many ways to use (MetagenomeScope)[https://mgsc.umiacs.io], but the easiest to to access the viewer interface at [mgsc.umiacs.io](https://mgsc.umiacs.io/).

Once you've opened the viewer interface, you can select a file to visualize
from your computer using the "Demo .db" button. The "Demo .db" button can be used to load demo files already available on this instance of
MetagenomeScope's viewer interface.

After loading a graph file into MetagenomeScope's viewer interface, a few controls become enabled:

- The "Assembly info" button
    - This button opens a dialog showing some of the information about this
    graph we got from SQLite3 earlier.
- The "Standard Mode" controls
    - These controls can be used to select and draw a connected component of the
    assembly graph.
    - Remember that MetagenomeScope sorts connected components of the graph in
    descending order by node count: so component 1 will contain the most nodes out
    of all the connected components in the graph. 

Select "Draw connected component" to draw a component of the graph!

## <a name="part1e1"> Example 1: Trial and error </a>

1. Open MetagenomeScope’s viewer interface at  mgsc.umiacs.io.
2. Pick a demo assembly graph using the “Demo .db” button.
3. Draw one of the graph’s connected components1!

## <a name="part1e2"> Example 2: Finding a frayed rope </a>

1. Open MetagenomeScope’s viewer interface at  mgsc.umiacs.io.
2. Pick demo assembly graph: `Shakya et al. metagenome, simplified (GML, 379 contigs, 229 edges)`
3. Find a frayed rope and signal when you are done.

## <a name="part1e3"> Example 3: SPQR </a>

1. Open MetagenomeScope’s viewer interface at  mgsc.umiacs.io.
2. Open the “MaryGold Paper Figure 2 graph” demo graph in  MetagenomeScope’s viewer interface
3. Draw the “explicit” and “implicit” SPQR tree graphs, and fully expand the  graph display by right-clicking metanodes


## <a name="part1e3"> Example 4: Find an interesting pattern in an HMP dataset </a>

1. Open MetagenomeScope’s viewer interface at  mgsc.umiacs.io.
2. Open the “SRS049959: post-clean (GML, 28,064 contigs, 21,769 edges)” demo graph in  MetagenomeScope’s viewer interface
3. Use what you have learned to explore around and post screenshot of pattern to #Stamps2019 when you are ready (optional)

# <a name="part2">Part 2: MetaCompass (Victoria Cepeda)</a>    
========

-- I have a set of metagenomic reads, and want to perform reference-guided assembly. 

    python3 go_metacompass.py -P [read1.fq,read2.fq] -l [max read length]-o [output_folder] -m [min coverage] -t [ncpu]

-- I know the reference genomes, or I want to perform comparative assembly for a particular genome.

    python3 go_metacompass.py -r [references.fasta] -P [read1.fq,read2.fq] -o [output_folder] -m [min coverage] -t [ncpu]

# DOCKER FUN

-- Where do I save my output?

    /output

-- How do I exit the container?

    exit

-- How do I access my output after exiting the container?

    cd /home/*YOUR SEA ANIMAL*/chiron/metacompass

# EXAMPLES

# <a name="part2e1">Example 1: Reference-guided assembly with known reference genomes (no reference selection).</a>
-- Input data is available in the tutorial folder :

    Reference genome file:  Candidatus_Carsonella_ruddii_HT_Thao2000.fasta
    Metagenomic reads:      thao2000.1.fq
                            thao2000.2.fq	
-- Run:
    cd /output
    python3 /opt/MetaCompass/go_metacompass.py -r /opt/MetaCompass/tutorial/Candidatus_Carsonella_ruddii_HT_Thao2000.fasta -P /opt/MetaCompass/tutorial/thao2000.1.fq,/opt/MetaCompass/tutorial/thao2000.2.fq -l 150 -o example1_output -m 1 -t 4


--Checking the output

    wc -m ./example1_output/metacompass.final.ctg.fa
    grep -c ">" ./example1_output/metacompass.final.ctg.fa

--Output folder contains the following files:

    - Assembled contigs:
        ./example1c_output/metacompass_output/metacompass.final.ctg.fa

    - Bowtie2, Megahit, and PILON logs:
        ./example1c_output/metacompass_logs/*.log

# <a name="part2e2"> Example 2: Reference-guided assembly of synthetic dataset with reference selection.</a>

The original Shakya et al. 2013 dataset is available online as [SRR606249](https://www.ebi.ac.uk/ena/data/view/SRR606249). The subsampled dataset, which was used as the tutorial example, can be downloaded via:

    wget https://obj.umiacs.umd.edu/stamps2019/Shakya_1M.fq.gz -O SRR606249_subset10_1.fq.gz
    wget https://obj.umiacs.umd.edu/stamps2019/Shakya_2M.fq.gz -O SRR606249_subset10_2.fq.gz

More information about how the subsampled dataset was generated can be found [here](https://github.com/signaturescience/metagenomics/tree/master/workflows/dataset_construction).

-- Unzip:

    gzip -d SRR606249_subset10_1.fq.gz
    gzip -d SRR606249_subset10_2.fq.gz

-- Run:
 
    cd /output
    python3 /opt/MetaCompass/go_metacompass.py -P SRR606249_subset10_1.fq,SRR606249_subset10_2.fq -t 8 -l 200 -o example2_output

--Output folder contains the following files:
    
    1) metacompass_output folder:
         - Assembled contigs:
                metacompass_output/metacompass.final.ctg.fa
         - Selected Reference genomes sequences:
                metacompass_output/metacompass.recruited.fa  
         - Selected Reference genomes ids and taxonomy ids
                metacompass_output/metacompass.recruited.ids

 
--Extracted recruited genome (you will need this later on):
    mkdir parsnp_input
    grep -A 23873 ">NC_018092.1 Pyrococcus furiosus COM1, complete genome" ./example2_output/metacompass_output/metacompass.recruited.fa > ./parsnp_input/NC_018092.ref.fa

# <a name="part2e3">Example 3: Reference-guided assembly of HMP dataset with reference selection.</a>

-- Download metagenomic sample:

    cd /output
    wget https://osf.io/a5ekr/download -O SRS049959_R1.fq.gz
    wget https://osf.io/daxze/download -O SRS049959_R2.fq.gz 

-- Unzip:

    gzip -d SRS049959_R1.fq.gz
    gzip -d SRS049959_R2.fq.gz

-- Run:
 
    python3 /opt/MetaCompass/go_metacompass.py -P SRS049959_R1.fq,SRS049959_R2.fq -t 8 -l 200 -o example3_output

-- Inspect:

    more ./example3_output/metacompass_output/metacompass.recruited.ids 
    cp ./example3_output/metacompass.final.ctg.fa ./parsnp_input/NC_021030.ref.fa

# <a name="part3">Part 3: Strain-level analyses with ParSNP </a>
========

This tutorial is to go over how to use ParSNP for strain-level analyses of genomes. The first dataset is a MERS coronavirus outbreak dataset involving 49 isolates.
The second dataset is a selected set of 31 Streptococcus pneumoniae genomes. The third dataset is  Both of these datasets should run on modestly equipped laptops in a few minutes.

   0) Set up path and fix permissions

    export PATH=$HOME/Harvest-Linux64-v1.1.2/parsnp:$PATH
    cd /home/*YOUR SEA ANIMAL*/chiron/metacompass
    sudo chmod -R g+rw ./

   1) <a name="part3e1">Example 1: 49 MERS Coronavirus genomes </a>
   
      * Download genomes: 
      
         *  cd /home/*YOUR SEA ANIMAL*/chiron/metacompass
         * `wget https://github.com/marbl/harvest/raw/master/docs/content/parsnp/mers49.tar.gz` [download](https://github.com/marbl/harvest/raw/master/docs/content/parsnp/mers49.tar.gz)
         * `tar -xvf mers49.tar.gz`
    
      * Run parsnp with default parameters 
      
         parsnp -g ./ref/EMC_2012.gbk -d ./mers49 -c
         
      * Command-line output 

        ![merscmd](https://github.com/marbl/harvest/raw/master/docs/content/parsnp/run_mers.cmd1.png?raw=true)

      * Visualize with Gingr [download](https://github.com/marbl/harvest/raw/master/docs/content/parsnp/run_mers.gingr1.ggr)
      
        ![mers1](https://github.com/marbl/harvest/raw/master/docs/content/parsnp/run_mers.gingr1.png?raw=true)

      * Configure parameters
      
         - 95% of the reference is covered by the alignment. This is <100% mainly due to a 1kbp unaligned region from 26kbp to 27kbp.
         - To force alignment across large collinear regions, use the `-C` maximum distance between two collinear MUMs::
         
            ./parsnp -g ./ref/EMC_2012.gbk -d ./mers49 -C 1000 -c
            
      * Visualize again with Gingr :download:`GGR <run_mers.gingr2.ggr>`
      
         - By adjusting the `-C` parameter, this region is no longer unaligned, boosting the reference coverage to 97%.

        ![mers2](https://github.com/marbl/harvest/raw/master/docs/content/parsnp/run_mers.gingr2.png?raw=true)
        
      * Zoom in with Gingr for nucleotide view of region
      
         - On closer inspection, a large stretch of N's in Jeddah isolate C7569 was the culprit
         
        ![mers3](https://github.com/marbl/harvest/raw/master/docs/content/parsnp/run_mers.gingr3.png?raw=true)
         
      * Inspect Output:
      
         * Multiple alignment: :download:`XMFA <runm1.xmfa>` 
         * SNPs: :download:`VCF <runm1.vcf>`
         * Phylogeny: :download:`Newick <runm1.tree>`
 
   2) <a name="part3e2">Example 2: 31 Streptococcus pneumoniae genomes </a>
   
     --Download genomes:
    
    wget https://github.com/marbl/harvest/raw/master/docs/content/parsnp/strep31.tar.gz
    tar -xvf strep31.tar.gz
    
     --Run parsnp:
      
    parsnp -r ./strep31/NC_011900.fna -d ./strep31 -p 8

     --Force inclusion of all genomes (-c):
      
    parsnp -r ./strep31/NC_011900.fna -d ./strep31 -p 8 -c

     --Enable recombination detection/filter (-x):
      
    parsnp -r ./strep31/NC_011900.fna -d ./strep31 -p 8 -c -x

     --Inspect Output:
      
         * Multiple alignment: parsnp.xmfa
         * Phylogeny: parsnp.tree

   3) <a name="part3e3">Example 3: Alistipes </a>

  --Download genomes:

    cd /home/*YOUR SEA ANIMAL*/chiron/metacompass
    mkdir alistipes_genomes
    cd alistipes_genomes
    wget http://tiny.cc/v5ggaz -O ali1.fna.gz
    wget http://tiny.cc/u6ggaz -O ali2.fna.gz
    wget http://tiny.cc/67ggaz -O ali3.fna.gz
     
 --Extract:

    gzip -d *.gz
    
 --Run parsnp:
      
    parsnp -r ./alistipes_genome/ali1.fna -d ./alistipes_genomes -o alistipes_parsnp_output -p 8
         
 --Inspect Output:

    more ./alistipes_parsnp_output/parsnpAligner.log   
    more ./alistipes_parsnp_output/parsnp.xmfa

 --Prepare output for AliView:

    harvesttools -i ./alistipes_parsnp_output/parsnp.ggr -M aliview.input.mfa

  4) <a name="part4"> Part 4: AliView and format conversion </a>


This tutorial is simply to highlight the ability to inspect strain-level differences within genomes assembled from metagenomic samples.

1) Use AliView 

    * Download AliView:

    [https://ormbunkar.se/aliview/#DOWNLOAD](https://ormbunkar.se/aliview/#DOWNLOAD)

    * Download MFA file:

    wget https://obj.umiacs.umd.edu/stamps2019/aliview.input.mfa

    * Open AliView
      
    * Load MFA file:

    File->Open File

